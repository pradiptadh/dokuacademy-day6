package task1;

public class BilPrima {
    public static boolean primeNumber(Integer number){
        if (number < 2) {
            return false;
        }
        int sqrtNumber = (int) Math.sqrt((double)number);
        for(int i = 2; i <= sqrtNumber; i++){
                if(number%i == 0){
                    return false;
                }
        }
        return true;
    }
}
