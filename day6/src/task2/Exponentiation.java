package task2;

public class Exponentiation {
    public static int pow(int x , int y){
        //ini menjadi o(log n)
         if(y == 0) return 1;
        return (y % 2 == 0) ?
                pow(x, y / 2) * pow(x, y / 2)
                : x * pow(x, (y - 1) / 2) *
                pow(x, (y - 1) / 2);


        //math.pow jadi o(1) tinggal dipilih mau make yang mana tinggal dikomen
       /* double hasil = Math.pow(x,y);
           int result = (int)hasil;
           return result;
        */
    }

}
