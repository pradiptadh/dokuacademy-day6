import task1.BilPrima;
import task2.Exponentiation;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan Pilihan Task");
        System.out.println("1. Task 1 ");
        System.out.println("2. Task 2 ");
        System.out.println("Masukkan Pilihan : ");
        int choice = input.nextInt();

        switch (choice){
            case 1 :
                BilPrima bilPrima = new BilPrima();
                System.out.println(bilPrima.primeNumber(1000000007));
                System.out.println(bilPrima.primeNumber(13));
                System.out.println(bilPrima.primeNumber(17));
                System.out.println(bilPrima.primeNumber(20));
                System.out.println(bilPrima.primeNumber(35));
                break;
            case 2:
                Exponentiation exponentiation = new Exponentiation();
                System.out.println(exponentiation.pow(2,3));
                System.out.println(exponentiation.pow(5,3));
                System.out.println(exponentiation.pow(10,2));
                System.out.println(exponentiation.pow(2,5));
                System.out.println(exponentiation.pow(7,3));
                break;
            default:
                System.out.println("Maaf format yang anda input salah!");
        }

    }

}